import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.font_manager as mf
import seaborn as sns
from scipy.stats import expon


class ScorePresenter:
    MIN_NB_SCORES_FOR_ESTIMATOR = 3

    def __init__(self, scores: pd.DataFrame):
        self.scores = scores.round(2)
        mf.fontManager.addfont("./assets/TaipeiSansTCBeta-Regular.ttf")
        mpl.rc("font", family="Taipei Sans TC Beta")

    def export_boxplot(self) -> None:
        scores = self.scores[self.scores.groupby("Estimator")["Estimator"].transform("count") >= ScorePresenter.MIN_NB_SCORES_FOR_ESTIMATOR]
        order = scores.groupby("Estimator")["Score"].mean().sort_values().index
        hue = scores["Estimator"].apply(lambda estimator: scores[scores["Estimator"] == estimator]["Score"].mean())
        plt.figure(figsize=(8, 5))
        palette = sns.cubehelix_palette(start=-.2, rot=.6, reverse=True, as_cmap=True)
        plt.subplots_adjust(left=0.28, right=0.98, top=0.94, bottom=0.06)
        sns.boxplot(data=scores, x="Score", y="Estimator", orient="h", order=order, hue=hue, legend=False, palette=palette, fliersize=0)
        plt.ylabel(None)
        plt.xlabel(None)
        plt.title("Outlier scores")
        plt.savefig("./images/estimator_scores.png")

    def export_distribution(self) -> None:
        plt.figure(figsize=(6, 2))
        # actual distribution
        sns.histplot(data=self.scores, x="Score", binrange=(0, 9), binwidth=1, stat="density", label="Actual distribution", color="#e0d2ba")
        # theoretical distribution: score is distance to median of Laplace(0, 1)
        x = np.linspace(0, 9, 1000)
        theoretical_y = expon.pdf(x, loc=0, scale=1)  # score ~ Exp(1) because X ~ Laplace(0, b) => |X| ~ Exp(1/b)
        sns.lineplot(x=x, y=theoretical_y, label=f"Theoretical distribution", color="#503361")
        # styling
        plt.xlim(0, 9)
        plt.ylabel(None)
        plt.xlabel(None)
        plt.title("Score distribution")
        plt.legend()
        plt.savefig("./images/score_distribution.png")
