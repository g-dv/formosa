# Identifying outlier-prone estimators

[[_TOC_]]

## 1. Method

Our objective is to develop a scoring mechanism to measure an estimator's tendency to produce estimates that significantly deviate from those of its peers.

### 1.1. Thought process

We have multiple sets of estimates for various events, and the first challenge is to standardize these samples. Therefore, our primary focus is on choosing a centering process and a good normalization term.

Subsequently, the score for an estimator can be determined as the average of distances between its standardized predictions and 0. The choice of the distance function remains to be defined.

#### 1.1.1. Mean-centering vs median-centering

Since the distribution of estimates won't always be symmetrical, the _mean_ and the _median_ may differ.

Finding if an estimate is an outlier often relies on the calculation of the "standardized mean difference", which quantifies the number of standard deviations separating an estimate from the mean.

However, there's no reason to believe that estimates, that aren't independent from one another, follow normal distributions.

Moreover, a limitation of this method lies in the sensitivity of the mean to outlier values. Consequently, given the nature of our data, the deviation from the mean may prove to be an inadequate indicator of the quality of an estimate.

For instance, let's imagine a hypothetical scenario in which we have estimates for the size of an army. The distribution of these estimates is depicted by the curve below. In this example, considering the mean as a superior estimator to the median may be viewed as an instance of the "golden mean fallacy". Centering around the median would make more sense.

<div align="center">
  <img src="./images/mean_vs_median.png" alt="Results" />
</div>

Furthermore, since the estimates are not independent and identically distributed:
- We expect many estimates to be equal to the median, while fewer estimates will be equal to the mean.
- We don't care about the z-score or the relationship with confidence intervals.

#### 1.1.2. MAD

We've established that the median serves as an outlier-resilient estimator. Thus, normalizing by the median absolute deviation (MAD) is appealing, partly because under the unrealistic assumption of a normally distributed population, $1.4826 \cdot \text{MAD}$ serves as an estimator of the standard deviation.

However, this approach has a major limitation in that MAD can be zero even when not all sample values are equal to the median, rendering normalization impossible (for example, the MAD for the sample $[195, 200, 200, 200, 202]$ is equal to 0, leading to the normalized estimates being all equal to $\infty$).

#### 1.1.3. Heavy tails

The distribution of estimates tends to have heavy tails, and this is a characteristic that we would like to preserve post-standardization. That's why we will avoid taking the square root of the sum of the squared differences as a normalization term.

Furthermore, after standardization, it seems excessive to use squared distances to compute the score.

#### 1.1.4. Final choice

To summarize the points made above, our objectives are as follows:
- Centering around the median.
- Avoiding squared distances.
- Normalizing using the mean of deviations, rather than the median of deviations.

Therefore, we opt for the following choices:
- Centering around the median.
- Using the absolute deviation from the median as the distance metric.
- Using the mean absolute deviation from the median for normalization.

It is noteworthy that this corresponds to the standardization process of a Laplace($\mu$, $b$) distribution.

Even though it wouldn't make sense to claim that the estimates follow a Laplace distribution since they are not independent and identically distributed, we can still plot the standardized estimates and draw a Laplace(0, 1) distribution on top:

<div align="center">
  <img src="./images/standardized_distribution.png" alt="Results" />
</div>

The resulting scores are interpretable, based on a robust estimator of the actual values, and can be compared from one distribution to the other.

Finally, unlike algorithms such as isolation forests, this formula is not a black box. This is crucial for building confidence in the results

### 1.2. Limitations

The median may not be a reliable estimator of the true value. Thus, good estimators won't necessarily obtain low outlier scores.

Moreover, the larger a group of estimators sharing the same information source, the less likely they are to be considered outliers, even if their source is flawed.

### 1.3. Formula

We first define a metric that evaluates, for a given set of comparable estimates, the extent to which one of the estimates deviates from the typical values.

Subsequently, we average the results of this intermediate metric for all of an estimator's estimates to establish the estimator's outlier score. The higher the deviation from the norm, the higher the final score.

#### 1.3.1. Score for an estimate

Let $x$ be a sample, and let $x_k \in x$.

We aim to define the function $\text{estimate\_outlier\_score}({x, x_k})$ that would tell if $x_k$ is a "typical" value of $x$ or not. The higher the deviation from the norm, the higher the metric will register.

We define $\hat{\mu}$ as follows:

```math
\hat{\mu} = \text{med}(x)
```

We define the normalization term $\hat{b}$ as the mean absolute deviation from the median:

```math
\hat{b} = \frac{1}{\lvert x \rvert} \sum_{i=1}^{\lvert x \rvert} \lvert x_i - \hat{\mu} \rvert
```

The score for $x_k$ is then defined as:

```math
\begin{array}{llll}
\text{estimate\_outlier\_score} : & \mathbb{R}^n \times \mathbb{R} & \to & \mathbb{R}^+ \\
& x, x_k & \mapsto & \frac{\lvert x_k - \hat{\mu} \rvert}{\hat{b}}
\end{array}
```

This score is zero when the evaluated value equals the median and increases as the value deviates from the median.

***Remark**: It should be noted that if the sample $x$ was generated by a random variable following Laplace($\mu$,$b$), then the corresponding scores are generated by a random variable following Exp(1). This is true because $\hat{\mu}$ and $\hat{b}$ are the maximum likelihood estimators for $\mu$ and $b$ respectively.*

#### 1.3.2. Score for an estimator

Let $E$ be the set of all the estimators\
Let $e$ be an estimator\
Let $X$ be the set of the samples that contain an estimate made by $e$\
Let $x_e$, be the estimate from $x$ that was made by $e$, with $x \in X$

We aim to define the function $\text{estimator\_outlier\_score}({x, x_e})$ that would reflect the tendency of $e$ to provide estimates that diverge from the typical values. The higher the deviation from the norm, the higher the metric will register.

The score for estimator $e$ is then defined as:

```math
\begin{array}{llll}
\text{estimator\_outlier\_score} : & \{ \mathbb{R}^n, \mathbb{R}^n, ..., \mathbb{R}^n \} \times E & \to & \mathbb{R}^+ \\
& X, e & \mapsto & \frac{1}{\lvert X \rvert}\sum_{x \in X} \text{estimate\_outlier\_score}({x, x_e})
\end{array}
```

This score is zero when the estimator's estimates are always equal their sample's median, and it increases as the estimator deviates from the typical estimations.

## 2. Data

The data can be visualized [here](./data/data.md).
And the corresponding `.h5` file can be found [here](./data/data.h5).

## 3. Results

### 3.1. Distribution

One may examine the distribution of scores, all estimators combined.

<div align="center">
  <img src="./images/score_distribution.png" alt="Results" />
</div>

We superimpose the theoretical distribution we would have had if the estimates were independent and identically distributed and followed Laplace distributions.

As:
```math
X \sim \text{Laplace(0, b)} \Rightarrow \lvert X \rvert \sim \text{Exp}(b^{-1})
```
Then we would have:
```math
\text{score} \sim \text{Exp}(1)
```

### 3.2. Outlier-prone estimators

**Reading tips:**
- The further to the right a box is, the more abnormal the estimates (with '*abnormal*' meaning '*far from the median estimate*').
- The greater the width of a box, the greater the variability in the quality of estimates provided by the represented estimator.

And, of course, being outlier-prone doesn't necessarily imply making bad estimates.

<div align="center">
  <img src="./images/estimator_scores.png" alt="Results" />
</div>
