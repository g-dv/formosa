if __name__ == "__main__":
    from step import Step
    with Step("Importing dependencies"):
        import pandas as pd
        from scorer import Scorer
        from score_presenter import ScorePresenter
        from misc_image_generator import MiscImageGenerator
    with Step("Loading data"):
        df = pd.read_hdf("data/data.h5", key="data")
    with Step("Computing scores"):
        estimators = pd.unique(df["Estimator"])
        scores = Scorer(df).build_scores_df_for_estimators(estimators)
    with Step("Exporting scores"):
        score_presenter = ScorePresenter(scores)
        score_presenter.export_boxplot()
        score_presenter.export_distribution()
    with Step("Export mean vs median image"):
        MiscImageGenerator.export_mean_vs_median_image()
    with Step("Export standardized estimates image"):
        MiscImageGenerator.export_estimates_distribution_image(df)
