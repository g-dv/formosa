from colorama import Fore
from colorama import Style


class Step:
    def __init__(self, name: str):
        print(name, end="... ", flush=True)

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        print(f"{Fore.GREEN}✓{Style.RESET_ALL}")
