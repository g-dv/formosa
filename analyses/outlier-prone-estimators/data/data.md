<table class="dataframe">
  <thead>
    <tr>
      <th>Estimator</th>
      <th>Estimate</th>
      <th colspan="4" halign="left">Focus</th>
    </tr>
    <tr>
      <th></th>
      <th></th>
      <th>Type</th>
      <th>Army</th>
      <th>Date</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Mu, Tushan 穆圖善</td>
      <td>6700</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1883-12</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>12000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1883-12</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>1000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-07</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Fournier, François</td>
      <td>600</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-07</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>2000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Rouil, Christope</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>1000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Julaude, Clément-Jean</td>
      <td>2000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Morel, Pierre-Marie</td>
      <td>3000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>3500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Billot, Albert</td>
      <td>15000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>6000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>15000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>5000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>6000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Davidson, James W.</td>
      <td>6000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Rubinstein, Murray A.</td>
      <td>16500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Hsu Yu-liang 許毓良</td>
      <td>13500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>12500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>12500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>3000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>2000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Billot, Albert</td>
      <td>2400</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>3000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>1000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>1000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ragot, Ernest</td>
      <td>10000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dargène, Jean</td>
      <td>3000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ke, Shexie 柯設偕</td>
      <td>2500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>2200</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>2200</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Gordon, Leonard H.</td>
      <td>600</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>3000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Ragot, Ernest</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Davidson, James W.</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Hsu Yu-liang 許毓良</td>
      <td>21000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Gayardon, Charles de</td>
      <td>5000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Mackay, George L.</td>
      <td>10000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>10000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>30000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>13000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>6000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>2000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>9000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>9000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Davidson, James W.</td>
      <td>3500</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>9000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>10000</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>3000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>2400</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>2000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Julien, Félix</td>
      <td>6000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Dargène, Jean</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>1500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>2500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>1500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>3500</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Mackay, George L.</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>De Fleurac, Adolphe</td>
      <td>16473</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1893</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>Davidson, James W.</td>
      <td>120</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Woods, George W.</td>
      <td>130</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>120</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>1000</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Hsu Yu-liang 許毓良</td>
      <td>450</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>450</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>300</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>130</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Julaude, Clément-Jean</td>
      <td>193</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>200</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Hsu Yu-liang 許毓良</td>
      <td>3000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>2000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>1500</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Larminat, Joseph de</td>
      <td>1600</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>2250</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>1800</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Thirion, Paul-Anatole</td>
      <td>1800</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>1750</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>700</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Rollet de L'Isle, Maurice</td>
      <td>625</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>650</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Billot, Albert</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>400</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ragot, Ernest</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dargène, Jean</td>
      <td>300</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ledu, Jean</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ke, Shexie 柯設偕</td>
      <td>750</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Hart, Robert</td>
      <td>800</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>600</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Lorraine, Gazette de</td>
      <td>1600</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>1440</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>800</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>400</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Julien, Félix</td>
      <td>1000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Dargène, Jean</td>
      <td>400</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Pen, Tsou-Héou</td>
      <td>1000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>5500</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>4000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Mackay, George L.</td>
      <td>8000</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>3021</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>150</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>100</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>60</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>150</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>90</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ke, Shexie 柯設偕</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>80</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Gayardon, Charles de</td>
      <td>98</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>600</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>300</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>350</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>350</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>350</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>350</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Julien, Félix</td>
      <td>300</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Cordier, Henri</td>
      <td>350</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>400</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>1</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Woods, George W.</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>15</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lespès, Sébastien</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Rollet de L'Isle, Maurice</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Mounier-Kuhn, Alain</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Billot, Albert</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lung, Chang 龍章</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Yeh, Chen-huei 葉振輝</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Julien, Félix</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>390</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>100</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Boulineau, Thomas</td>
      <td>16</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Lian, Heng 連橫</td>
      <td>80</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>20</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Wikipedia</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Mounier-Kuhn, Alain</td>
      <td>18</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Billot, Albert</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>100</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>15</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Cordier, Henri</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Ke, Shexie 柯設偕</td>
      <td>400</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Lung, Chang 龍章</td>
      <td>21</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Peng, Jinan 彭紀南</td>
      <td>550</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Zhang, Jianlong 張建隆</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Hsu Yu-liang 許毓良</td>
      <td>20</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>Dodd, John</td>
      <td>20</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-11</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Keller, Edmond</td>
      <td>27</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Le Temps</td>
      <td>9</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Lung, Chang 龍章</td>
      <td>41</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>20</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>20</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>Coppin, René</td>
      <td>18</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>16</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>Duboc, Emile</td>
      <td>11</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>17</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>15</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>41</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>48</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Davidson, James W.</td>
      <td>100</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>41</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Lung, Chang 龍章</td>
      <td>56</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>41</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>45</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>Dartige du Fournet, Louis</td>
      <td>2</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Garnot, Eugène</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Loir, Maurice</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Anonyme</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>De Lonlay, Dick</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Julien, Félix</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Cordier, Henri</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Ledu, Jean</td>
      <td>3</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Gaultier, Alphonse</td>
      <td>5</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>Courbet, Amédé-Anatole</td>
      <td>4</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
  </tbody>
</table>