import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.stats import laplace

from scorer import Scorer


class MiscImageGenerator:
    @staticmethod
    def export_mean_vs_median_image() -> None:
        plt.figure(figsize=(5, 2))
        x = np.linspace(0, 100, 1000)

        d1 = {"loc": 15, "scale": 3}
        d2 = {"loc": 85, "scale": 3}
        n1 = 400
        n2 = 100

        y1 = norm.pdf(x, **d1)
        y2 = norm.pdf(x, **d2)
        y = (n1 * y1) + (n2 * y2)

        sample = np.concatenate([
            norm.rvs(size=n1, **d1),
            norm.rvs(size=n2, **d2)
        ])

        sns.lineplot(x=x, y=y, color="#503361")
        plt.axvline(x=np.median(sample), color="#b16c85", linestyle="dashed", label="median")
        plt.axvline(x=np.mean(sample), color="#b16c85", linestyle="dotted", label="mean")
        plt.xlim(0, 100)
        plt.xticks([])
        plt.yticks([])
        plt.legend()
        plt.savefig("./images/mean_vs_median.png")

    def export_estimates_distribution_image(df: pd.DataFrame) -> None:
        def _normalize_estimates(focus: pd.Series, df: pd.DataFrame) -> pd.Series:
            filtered_df = df.loc[(df["Focus"] == focus).all(axis=1)]
            x = filtered_df["Estimate"]
            b = Scorer.get_b(x)
            if not Scorer.can_score(filtered_df) or b == 0:
                return pd.Series([])
            return (x - x.median()) / b

        plt.figure(figsize=(6, 2))
        # actual distribution
        focuses = df["Focus"].drop_duplicates()
        actual_y = focuses.apply(_normalize_estimates, df=df, axis=1).values.ravel()
        sns.histplot(x=actual_y, binwidth=1, binrange=(-9.5, 9.5), stat="density", label="Actual distribution", color="#e0d2ba")
        # theoretical distribution
        x = np.linspace(-9, 9, 1000)
        y = laplace.pdf(x, loc=0, scale=1)
        sns.lineplot(x=x, y=y, label=f"Laplace(0, 1)", color="#503361")
        # styling
        plt.xlim(-9, 9)
        plt.ylabel(None)
        plt.xlabel(None)
        plt.title("Standardized estimates")
        plt.legend()
        plt.savefig(f"images/standardized_distribution.png")
