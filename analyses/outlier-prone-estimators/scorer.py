import pandas as pd
import numpy as np


class Scorer:
    MIN_NB_ESTIMATES_TO_COMPUTE_SCORE = 3

    def __init__(self, df: pd.DataFrame):
        self.df = df

    @staticmethod
    def get_b(x: pd.Series) -> float:
        # compute mean absolute deviation from the median
        return (x - x.median()).abs().sum() / x.shape[0]

    @staticmethod
    def _compute_normalized_median_difference(series: pd.Series, value: float) -> float:
        # the normalization term is `b`, from the `Laplace(mu, b)` distribution
        b = Scorer.get_b(series)
        if b == 0:
            return 0
        return abs(value - series.median()) / b

    @staticmethod
    def can_score(estimates: pd.DataFrame) -> bool:
        if estimates.shape[0] < Scorer.MIN_NB_ESTIMATES_TO_COMPUTE_SCORE:
            return False  # not enough estimates to compute score
        estimators = estimates["Estimator"]
        if estimators.eq(estimators.iloc[0]).all():
            return False  # all estimates were made by the same estimator
        return True

    def _score(self, focus: pd.Series, estimator: str) -> float:
        estimates = self.df.loc[(self.df["Focus"] == focus).all(axis=1)]
        if not self.can_score(estimates):
            return np.nan
        series = estimates["Estimate"]
        value = (estimates[estimates["Estimator"] == estimator]["Estimate"]).mean()
        return self._compute_normalized_median_difference(series, value)

    def _build_scores_df_for_estimator(self, estimator: str) -> pd.DataFrame:
        focuses = self.df[self.df["Estimator"] == estimator]["Focus"].drop_duplicates()
        scores_series = focuses.apply(self._score, estimator=estimator, axis=1)
        return scores_series.dropna().rename("Score").to_frame().assign(Estimator=estimator)

    def build_scores_df_for_estimators(self, estimators: pd.Series) -> pd.DataFrame:
        return pd.concat([self._build_scores_df_for_estimator(estimator) for estimator in estimators])
