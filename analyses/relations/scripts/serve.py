from os.path import dirname
from os import chdir
from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer


class MyHandler(SimpleHTTPRequestHandler):
    def do_GET(self):
        # Mimic the _redirects file
        self.path = self.path.replace("/formosa/fr", "/formosa/build/fr-FR/index.html")
        self.path = self.path.replace("/formosa/zh", "/formosa/build/zh-TW/index.html")

        # Gitlab Pages urls look like 'https://user.gitlab.io/project/page' and the
        # served directory is `public/`. Thus, to run the website locally, we remap:
        # http://localhost:port/project/page -> http://localhost:port/public/page
        gitlab_project = "/formosa"
        gitlab_served_dir = "/public"
        if self.path.startswith(gitlab_project):
            self.path = gitlab_served_dir + self.path[len(gitlab_project):]

        super().do_GET()


port = 8000

# paths are computed from project's root
chdir(dirname(dirname(__file__)))

with TCPServer(("", port), MyHandler) as httpd:
    print(f"Serving on port {port}")
    httpd.serve_forever()
