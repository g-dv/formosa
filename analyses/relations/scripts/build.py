import json
from os.path import join
from os.path import dirname
from os import chdir

languages = ["fr-FR", "zh-TW"]
src_dir = "src"
files_to_compile = [
    "chart.js",
    "data.json",
    "index.html",
    "graph-modes.json",
    "pie-node.js",
]
translations_dir = join("src", "translations")
build_dir = join("public", "build")

# paths are computed from project's root
chdir(dirname(dirname(__file__)))

for language in languages:
    # read translations
    with open(join(translations_dir, language + ".json"), "r") as f:
        mappings = json.load(f)["mappings"]
    for input_file in files_to_compile:
        # read file to compile
        with open(join(src_dir, input_file), "r") as f:
            content = f.read()
        # replace the {{ placeholders }}
        for placeholder, translation in mappings.items():
            content = content.replace("{{ " + placeholder + " }}", translation)
        # write the resulting file in the build dir
        with open(join(build_dir, language, input_file), "w") as f:
            f.write(content)
