# Relations between various sources

**The directed graph is accessible...**
  * ...in French: **[link](https://g-dv.gitlab.io/formosa/fr)**
  * ...in Traditional Chinese: **[link](https://g-dv.gitlab.io/formosa/zh)**

The data and additional info are available in French on the website: [Once upon a Taïwan](https://donada.notion.site/donada/Once-upon-a-Ta-wan-ab245103b3a74996b797147e1346d3e0).
