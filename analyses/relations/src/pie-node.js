(() => {

  const _getOption = colorMask => ({
    animation: false,
    series: [{
      type: 'pie',
      top: -40,
      left: -40,
      width: 380,
      height: 380,
      emphasis: {
        scale: 0
      },
      label: {
        show: false
      },
      data: colorMask.map(isPresent => ({
        value: Number(isPresent) / colorMask.length
      }))
    }]
  });

  function _createCanvas() {
    const canvas = document.createElement('canvas');
    return canvas;
  }

  function getPieNodeImage(colorMask) {
    const canvas = _createCanvas();
    const chart = echarts.init(canvas, 'roma', { renderer: 'svg', width: 300, height: 300 });
    const option = _getOption(colorMask);
    chart.setOption(option);
    const base64 = chart.getDataURL();
    chart.dispose();
    return base64;
  }

  window.getPieNodeImage = getPieNodeImage;

})();
