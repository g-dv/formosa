(async () => {
  const BASE_NODE_SIZE = 8;
  const NODE_SIZE_INCREMENT_PER_CITATION = 1.5;
  const NODE_SIZE_INCREMENT_PER_SOURCE = 1.5;
  const DISABLED_COLOR = 'rgba(0, 0, 0, 0.3)';

  class Config {
    _updateFromDom() {
      this.nameFilter = document.getElementById('settings__filter-names-select').value;
      this.mode = this._graphModes.find(mode => mode.name === document.getElementById('settings__graph-mode-select').value);
      this.layout = document.getElementById('settings__layout-select').value;
    }

    constructor(graphModes) {
      this._graphModes = graphModes;
      this._updateFromDom();
    }
  }

  class GraphOptionMaker {
    _getNodes(rawNodes, config) {
      const mode = config.mode;
      return rawNodes.map(node => ({
        name: node['nom'],
        id: node['nom'],
        symbolSize: BASE_NODE_SIZE,
        itemStyle: {
          color: this._getNodeColor(node, mode)
        },
        category: this._getNodeCategory(node, mode),
        symbol: this._getNodeSymbol(node, mode),
        label: {
          show: !config.nameFilter || node[config.nameFilter],
          position: 'bottom',
          color: 'rgba(0, 0, 0, 0.5)',
          formatter: (params) => params.name.split(',')[0]
        }
      }));
    }

    _getLinks(rawLinks) {
      return rawLinks.map(link => ({
        source: link.source,
        target: link.target,
        lineStyle: {
          width: link.type === 1 ? 1 : 1.5,
          color: link.type === 1 ? 'rgba(0, 0, 0, 0.4)' : 'rgba(0, 0, 0, 0.8)',
          type: link.type === 1 ? 'dashed' : 'solid'
        }
      }));
    }

    _getOption(data, config) {
      const categories = this._getCategories(config.mode);
      categories.push({ name: config.mode.defaultCategory });
      const nodes = this._getNodes(data.nodes, config);
      const links = this._getLinks(data.links);
      for (const link of links) {
        nodes.find(node => node.id === link.target).symbolSize += NODE_SIZE_INCREMENT_PER_SOURCE;
        nodes.find(node => node.id === link.source).symbolSize += NODE_SIZE_INCREMENT_PER_CITATION;
      }
      const legend = this._getLegend(categories, config.mode);
      legend[legend.length - 1].itemStyle = { color: DISABLED_COLOR };

      const edgesLegend = [
        {
          name: "{{ legend.item.weak_ref.title }}",
          icon: 'image:///formosa/assets/img/legend_dashed_line.png'
        },
        {
          name: "{{ legend.item.strong_ref.title }}",
          icon: 'image:///formosa/assets/img/legend_solid_line.png'
        }
      ];
      return {
        tooltip: {},
        legend: [
          {
            data: legend,
            top: 10,
            selectedMode: false
          },
          {
            data: edgesLegend,
            right: 110,
            bottom: 13,
            tooltip: {
              show: true,
              formatter: () => `<b>{{ legend.item.strong_ref.title }}</b>: {{ legend.item.strong_ref.description }}<br><b>{{ legend.item.weak_ref.title }}</b>: {{ legend.item.weak_ref.description }}`
            }
          }
        ],
        nodeScaleRatio: 1,
        series: [
          {
            type: 'graph',
            draggable: true,
            layout: config.layout,
            force: {
              repulsion: 250,
              friction: 0.4,
              gravity: 0.2,
              initAnimation: 'circular'
            },
            roam: true,
            data: nodes,
            links: links,
            categories: categories,
            edgeSymbol: ['none', 'arrow'],
            edgeSymbolSize: 5,
            autoCurveness: true,
            emphasis: {
              scale: false,
              focus: 'adjacency',
              lineStyle: {
                width: 2
              },
              label: {
                show: false
              }
            }
          },
          {
            type: 'graph',
            categories: edgesLegend
          }
        ]
      };
    }
  }

  class MultiLabelGraphOptionMaker extends GraphOptionMaker {
    _getCategories(mode) {
      return mode.mappings.map(mapping => ({ name: mapping.category }));
    }

    _getNodeCategory(node, mode) {
      return mode.mappings.length; // in multi_label, all nodes actually belong to the same category
    }

    _getNodeSymbol(node, mode) {
      const colorMask = [];
      for (const mapping of mode.mappings) {
        colorMask.push(node[mapping.boolColumn]);
      }
      if (colorMask.every(e => !e)) {
        return 'circle';
      }
      return `image://${getPieNodeImage(colorMask)}`;
    }

    _getNodeColor(node, mode) {
      return DISABLED_COLOR; // in multi_label, all nodes actually belong to the same category
    }

    _getLegend(categories, mode) {
      return categories.map(cat => ({...cat}));
    }
  }

  class SimpleGraphOptionMaker extends GraphOptionMaker {
    _getCategories(mode) {
      return Object.keys(mode.colorMap).map(cat => ({ name: cat }))
    }

    _getNodeCategory(node, mode) {
      return node[mode.column].length ? node[mode.column] : mode.defaultCategory;
    }

    _getNodeSymbol(node, mode) {
      return 'circle';
    }

    _getNodeColor(node, mode) {
      return mode.colorMap[node[mode.column]] || DISABLED_COLOR;
    }

    _getLegend(categories, mode) {
      return categories.map(cat => (
        {...cat, itemStyle: { color: mode.colorMap[cat.name] }}
      ));
    }
  }

  function _updateGraph(graph, data, graphModes) {
    const config = new Config(graphModes);
    const graphOptionMaker = {
      simple: new SimpleGraphOptionMaker(),
      multi_label: new MultiLabelGraphOptionMaker()
    }[config.mode.dataEncoding];
    const option = graphOptionMaker._getOption(data, config);
    graph.setOption(option);
  }

  async function init() {
    const graphModes = (await fetch('/formosa/build/{{ main.language }}/graph-modes.json').then(response => response.json())).modes;
    const data = await fetch('/formosa/build/{{ main.language }}/data.json').then(response => response.json());
    const graph = echarts.init(document.getElementById('chart'), 'roma');
    const updateCallback = _updateGraph.bind(null, graph, data, graphModes);
    updateCallback();
    for (const selectElementId of ['settings__graph-mode-select', 'settings__filter-names-select', 'settings__layout-select']) {
      document.getElementById(selectElementId).addEventListener('change', updateCallback);
    }
    document.getElementById('language-switch').addEventListener('click', () => {
      window.location.href = '/formosa/' + ('{{ main.language }}' === 'fr-FR' ? 'zh' : 'fr');
    });
    window.onresize = () => graph.resize();
    // neutralize events when user hovers/clicks on the edges legend
    graph.on('legendselectchanged', params => graph.dispatchAction({
      type: 'legendSelect',
      name: params.name
    }));
    graph.on('highlight', params => graph.dispatchAction({
      type: 'downplay',
      name: params.name
    }));
  }

  init();

})();
