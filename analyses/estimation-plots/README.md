# Estimation plots

The dataset compiles various estimates made regarding the French and Chinese armies in Taiwan during the war. These estimates cover the number of casualties, the size of garrisons, and the number of soldiers involved in battles.

The notebook allows for filtering and aggregating different estimates on the same swarm plot.

In order to aggregate estimates made on different contexts, the exact numerical value of the estimates is sacrificed in favor of standardized values that allow for inter-contexts comparisons. The standardization process is explained [here](../outlier-prone-estimators/README.md).

The goal is to detect possible correlations or identify trends in overestimating or underestimating certain types of events. For instance, these visualizations highlight the tendency of Chinese estimators to overestimate the size and losses of the French army, as well as reveal the abnormal nature of General Liu Mingchuan's estimates.

## Running the notebook

### Demo

![screenshot](./images/demo.gif)

### Installation

#### Option 1: Google Colab
- Download the [notebook](./notebook.ipynb) and the [data.h5](./data/data.h5) file.
- Import the notebook on [colab.research.google.com](https://colab.research.google.com/).
- Run all cells.
- At some point you'll be asked to upload a file, upload the data.h5 file.

#### Option 2: Local execution

- Download Python 3.10 if you don't have it installed already.
- Using pip, install:
  - jupyter notebook
  - numpy
  - pandas
  - matplotlib
  - tables
  - ipywidgets
  - seaborn
- From your terminal, start `jupyter notebook` in the `estimation-plots` directory, and open the notebook.
- Run all cells.
