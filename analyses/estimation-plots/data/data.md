<table class="dataframe">
  <thead>
    <tr>
      <th>Estimator nationality</th>
      <th>Estimator status</th>
      <th>Estimator</th>
      <th>Estimate</th>
      <th>Estimation date &lt; 1940</th>
      <th colspan="4" halign="left">Focus</th>
    </tr>
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>Type</th>
      <th>Army</th>
      <th>Date</th>
      <th>Location</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Mu, Tushan 穆圖善</td>
      <td>6700</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1883-12</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>12000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1883-12</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Businessman</td>
      <td>Dodd, John</td>
      <td>1000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-07</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Fournier, François</td>
      <td>600</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-07</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>2500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>2000</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>2500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Rouil, Christope</td>
      <td>2500</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Civilian</td>
      <td>Woods, George W.</td>
      <td>2000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Press</td>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>1000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>2500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>2500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Julaude, Clément-Jean</td>
      <td>2000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Morel, Pierre-Marie</td>
      <td>3000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>2500</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>3500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>15000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>6000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>15000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Brière, Paul</td>
      <td>5000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>5000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>6000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>6000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Rubinstein, Murray A.</td>
      <td>16500</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Hsu Yu-liang 許毓良</td>
      <td>13500</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>12500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>12500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-08-01 to 1884-11-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>3000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>2500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>2000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>2400</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>3000</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>1000</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>2500</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>1000</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Ragot, Ernest</td>
      <td>10000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Dargène, Jean</td>
      <td>3000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Ke, Shexie 柯設偕</td>
      <td>2500</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>2200</td>
      <td>False</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>2200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Gordon, Leonard H.</td>
      <td>600</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>3000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>4000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Ragot, Ernest</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Hsu Yu-liang 許毓良</td>
      <td>21000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gayardon, Charles de</td>
      <td>5000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>CAN</td>
      <td>Civilian</td>
      <td>Mackay, George L.</td>
      <td>10000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>10000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>30000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Brière, Paul</td>
      <td>20000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>13000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>6000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1884-12-01 to 1885-03-01</td>
      <td>Northern Taiwan</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>2000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>9000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>9000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>3500</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>9000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>10000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>3000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>2400</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>2000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Julien, Félix</td>
      <td>6000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Dargène, Jean</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>2400</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>2400</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>3500</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>1500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>2500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>1500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>3500</td>
      <td>False</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>CAN</td>
      <td>Civilian</td>
      <td>Mackay, George L.</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>De Fleurac, Adolphe</td>
      <td>16473</td>
      <td>True</td>
      <td>Garrison</td>
      <td>Chinese</td>
      <td>1893</td>
      <td>Taiwan</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>120</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Civilian</td>
      <td>Woods, George W.</td>
      <td>130</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>120</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>1000</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Hsu Yu-liang 許毓良</td>
      <td>450</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>450</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>200</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Press</td>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>300</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>130</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Julaude, Clément-Jean</td>
      <td>193</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>200</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Hsu Yu-liang 許毓良</td>
      <td>3000</td>
      <td>False</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>2000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>1500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Larminat, Joseph de</td>
      <td>1600</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>2250</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>1800</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Thirion, Paul-Anatole</td>
      <td>1800</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>1750</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>700</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Lin, Pei-Hsi</td>
      <td>650</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Civilian</td>
      <td>Coppin, René</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Civilian</td>
      <td>Rollet de L'Isle, Maurice</td>
      <td>625</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>650</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Press</td>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>400</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Ragot, Ernest</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Dargène, Jean</td>
      <td>300</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Ledu, Jean</td>
      <td>600</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Ke, Shexie 柯設偕</td>
      <td>750</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia US</td>
      <td>600</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>600</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>600</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>600</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Politician</td>
      <td>Hart, Robert</td>
      <td>800</td>
      <td>True</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>600</td>
      <td>False</td>
      <td>Fighters</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Press</td>
      <td>Lorraine, Gazette de</td>
      <td>1600</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>1440</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>800</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Anonyme</td>
      <td>400</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>400</td>
      <td>False</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>400</td>
      <td>False</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>400</td>
      <td>False</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Julien, Félix</td>
      <td>1000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Dargène, Jean</td>
      <td>400</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Soldier</td>
      <td>Pen, Tsou-Héou</td>
      <td>1000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>5500</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>4000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CAN</td>
      <td>Civilian</td>
      <td>Mackay, George L.</td>
      <td>8000</td>
      <td>True</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>3021</td>
      <td>False</td>
      <td>Garrison</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>150</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>100</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>60</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Yeh, Chen-huei 葉振輝</td>
      <td>400</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>150</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>90</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>300</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>200</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>20</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>300</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>280</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>280</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>280</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>280</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>280</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>280</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>200</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Ke, Shexie 柯設偕</td>
      <td>180</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Soldier</td>
      <td>Sun, Kaihua 孫開華</td>
      <td>200</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>280</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gayardon, Charles de</td>
      <td>98</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>1000</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>2000</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>1750</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>1500</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>1500</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>1500</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shih-shan, Tsai 蔡石山</td>
      <td>1000</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>1050</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>1000</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>1000</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>600</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>300</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>350</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>350</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>350</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>300</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>300</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>300</td>
      <td>False</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>300</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>350</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Julien, Félix</td>
      <td>300</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Cordier, Henri</td>
      <td>350</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Soldier</td>
      <td>Pen, Tsou-Héou</td>
      <td>1000</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>400</td>
      <td>True</td>
      <td>Casualties</td>
      <td>Chinese</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>1</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Civilian</td>
      <td>Woods, George W.</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>15</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Lespès, Sébastien</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Civilian</td>
      <td>Rollet de L'Isle, Maurice</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Mounier-Kuhn, Alain</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Research</td>
      <td>Lung, Chang 龍章</td>
      <td>5</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Yeh, Chen-huei 葉振輝</td>
      <td>5</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>2</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-08</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Julien, Félix</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>390</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Politician</td>
      <td>Liu, Mingchuan 劉銘傳</td>
      <td>100</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Boulineau, Thomas</td>
      <td>16</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Brière, Paul</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Lian, Heng 連橫</td>
      <td>80</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>75</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>20</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia US</td>
      <td>17</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>17</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>17</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Mounier-Kuhn, Alain</td>
      <td>18</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Politician</td>
      <td>Billot, Albert</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Press</td>
      <td>Dianshizhai huabao 點石齋畫報</td>
      <td>100</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>15</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Cordier, Henri</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Ke, Shexie 柯設偕</td>
      <td>400</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Research</td>
      <td>Lung, Chang 龍章</td>
      <td>21</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Soldier</td>
      <td>Peng, Jinan 彭紀南</td>
      <td>550</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Zhang, Jianlong 張建隆</td>
      <td>17</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Hsu Yu-liang 許毓良</td>
      <td>20</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>TAI</td>
      <td>Research</td>
      <td>Shiu, Wen-tang 許文堂</td>
      <td>17</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-10</td>
      <td>Tamsui</td>
    </tr>
    <tr>
      <td>UK</td>
      <td>Civilian</td>
      <td>Dodd, John</td>
      <td>20</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1884-11</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Keller, Edmond</td>
      <td>27</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Press</td>
      <td>Le Temps</td>
      <td>9</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Research</td>
      <td>Lung, Chang 龍章</td>
      <td>41</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>20</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>20</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-1</td>
      <td>Keelung</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Rouault de Champglen</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Coppin, René</td>
      <td>18</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>16</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Duboc, Emile</td>
      <td>11</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>17</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Anonyme</td>
      <td>15</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-01-10</td>
      <td>Coup de main</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>41</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>48</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>USA</td>
      <td>Research</td>
      <td>Davidson, James W.</td>
      <td>100</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>41</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>CHI</td>
      <td>Research</td>
      <td>Lung, Chang 龍章</td>
      <td>56</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>41</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>45</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03-04 to 1885-03-07</td>
      <td>Le Cirque</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Dartige du Fournet, Louis</td>
      <td>2</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Garnot, Eugène</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Loir, Maurice</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>Anonyme</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Writer</td>
      <td>De Lonlay, Dick</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Julien, Félix</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Research</td>
      <td>Cordier, Henri</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia FR</td>
      <td>5</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia EN</td>
      <td>5</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>N/D</td>
      <td>Research</td>
      <td>Wikipedia CHI</td>
      <td>5</td>
      <td>False</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Ledu, Jean</td>
      <td>3</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Nicolas, Victor-Éleuthère</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Gaultier, Alphonse</td>
      <td>5</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
    <tr>
      <td>FRA</td>
      <td>Soldier</td>
      <td>Courbet, Amédé-Anatole</td>
      <td>4</td>
      <td>True</td>
      <td>Casualties</td>
      <td>French</td>
      <td>1885-03</td>
      <td>Pescadores</td>
    </tr>
  </tbody>
</table>