# Formosa

"[Once Upon a Taiwan](https://donada.notion.site/donada/Once-upon-a-Ta-wan-ab245103b3a74996b797147e1346d3e0)" has created a database that compiles information on the Franco-Chinese War of 1884-1885 in Taiwanese territory. This repository offers various tools to explore the datasets.

* **Showing the relations between various sources with a directed graph**
  - Graph can be seen here: [fr](https://g-dv.gitlab.io/formosa/fr) / [zh](https://g-dv.gitlab.io/formosa/zh)
  - Source code: [link](./analyses/relations)

* **Army sizes and losses: identifying outlier-prone estimators**
  - Results: [link](./analyses/outlier-prone-estimators/README.md?ref_type=heads#32-outlier-prone-estimators)
  - Data, code and analysis: [link](./analyses/outlier-prone-estimators)

* **Army sizes and losses: visualize trends**
  - Notebook, demo: [link](./analyses/estimation-plots/README.md)

Data, additional info, analyses and contextualization can be found [here](https://donada.notion.site/donada/Once-upon-a-Ta-wan-ab245103b3a74996b797147e1346d3e0) (in French).
